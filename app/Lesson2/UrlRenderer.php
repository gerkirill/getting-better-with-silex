<?php
namespace Lesson2;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class UrlRenderer {
    private $app;
    private $request;

    public function __construct($app, $request = null) {
        $this->app = $app;
        if (null == $request) {
            $this->request = Request::createFromGlobals();
        } else {
            $this->request = $request;
        }
    }

    public function render($url) {
        $subRequest = Request::create($url, 'GET', array(), $this->request->cookies->all(), array(), $this->request->server->all());
        if ($this->request->getSession()) {
            $subRequest->setSession($this->request->getSession());
        }
        $response = $this->app->handle($subRequest, HttpKernelInterface::SUB_REQUEST, false);
        return $response->getContent();
    }
} 