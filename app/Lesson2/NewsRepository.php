<?php
namespace Lesson2;

class NewsRepository {

    const ORDER_POPULAR = 'popular';
    const ORDER_LATEST = 'latest';

    private $news = array(array(
        'date' => '20130101',
        'popularity' => 10,
        'title' => 'Новость 1'
    ),array(
        'date' => '20130102',
        'popularity' => 9,
        'title' => 'Новость 2'
    ),array(
        'date' => '20130103',
        'popularity' => 8,
        'title' => 'Новость 3'
    ));


    public function getAllNews() {
        return $this->news;
    }

    public function getNewsOrdered($order, $limit) {
        $news = $this->news;
        usort($news, function($a, $b) use ($order) {
            if ($order == self::ORDER_LATEST) {
                return strnatcmp($b['date'], $a['date']);
            } elseif ($order == self::ORDER_POPULAR) {
                return strnatcmp($b['popularity'], $a['popularity']);
            }
        });
        return array_slice($news, 0, $limit);
    }
} 