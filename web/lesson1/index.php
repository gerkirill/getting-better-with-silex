<?php
use Silex\Application;

// это в общем первый и последний require, дальше работает автозагрузка
require(__DIR__ . '/../../vendor/autoload.php');

// вот так вот выглядит использование большинства микро-фреймворков:
$app = new Application();

// этот код определяет что выведется при обращении к домашней странице приложения
$app->get('/', function() {
    return 'Hello world'; 
});

// включим детальную информацию об ошибках
// $app['debug'] = true;

/*
Роутинг очень важен. Он позволяет разделить код в зависимости от урл. А разделять
код очень важно. Где может - роутинг берет на себя рутину получения переменных из URL, как
например тут - $name.
 */
$app->get('/hello/{name}', function($name) use ($app) {
    return 'Hello '.$app->escape($name);
});

/*
В функции можно объявить параметры с типом Application и Request, и фреймворк передаст их при
вызове обработчика.
*/
$app->get('/json', function(Application $app) {
    // Функция автодополнения среды отлично показывает что еще есть в переменной $app. Например json()
    return $app->json(array('status' => 'ok'));
});

/* прекращение выполнения - abort и redirect */
$app->get('/404', function() use ($app) {
    // если посмотреть в код $app->abort() - будет видно почему return здесь не нужен
    $app->abort('404', 'Not found!');
});

$app->get('/old-page', function() use ($app) {
    return $app->redirect('/');
});

$app->run();
/*
Суммарно рассмотрели:
 - роутинг
 - возвращение ответов разных типов (html, json, редирект, ошибка 404)
 == Простые вещи делаются просто ==
 Больше деталей здесь http://silex.sensiolabs.org/doc/usage.html
*/