<?php
use Lesson2\NewsRepository;
use Lesson2\UrlRenderer;
use Silex\Application;

require(__DIR__ . '/../../vendor/autoload.php');

$app = new Application();
$app['debug'] = true;

$navItems = array(
    'home' => 'Главная',
    'about' => 'О сайте',
    'blog' => 'Блог'
);

// составная страница - выводит данные доступные по трем другим роутам - /navigation/home,
// /news/latest/2 и /news/popular/1
// Все эти роуты можно тестировать и отдельно, открывая в браузере.
// Как бонус - эти части страницы можно обновлять через ajax
$app->get('/', function() use ($app) {
    // UrlRenderer - это самописный класс специально для этого примера, не часть фреймворка
    $renderer = new UrlRenderer($app);
    return
        '<h1>Очень сложная страница</h1>'
        . $renderer->render('/navigation/home')
        . '<h2>Последине новости</h2>'
        . $renderer->render('/news/latest/2')
        . '<h2>Популярные новости</h2>'
        . $renderer->render('/news/popular/1')
    ;
});

$app->get('/navigation/{current}', function($current) use ($app, $navItems) {
    $html = '';
    foreach($navItems as $key => $navItem) {
        if ($key == $current) {
            $html.= $app->escape($navItem);
        } else {
            $html.= " <a href='/$key'>{$app->escape($navItem)}</a> ";
        }
    }
    return $html;
});

$app->get('/news/{order}/{count}', function($order, $count) use ($app) {
    // самописный класс. класс который отвечает за получение чего-то - это как привило репозиторий.
    $newsRepository = new NewsRepository();
    $news = $newsRepository->getNewsOrdered($order, $count);
    $html = '';
    foreach ($news as $item) {
        $html .= "<div>" . $app->escape($item['title']) . "</div>";
    }
    return $html;
});

$app->run();
